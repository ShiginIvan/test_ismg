import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


@pytest.fixture(scope="function")
def browser():
    driver = webdriver.Remote(command_executor=('http://192.168.123.168:4444/wd/hub'),
                              desired_capabilities={'browserName': 'chrome', 'platform': 'WINDOWS'})
    driver.set_window_size(1920, 1080)
    yield driver
    driver.quit()

